﻿#include <iostream>
#include <string>
#include <fstream>
using namespace std;
struct etykieta
{
	string nazwa;
	etykieta* nastepna = nullptr;
};
struct ksiazka
{
	string autor;
	string tytul;
	ksiazka* nastepna = nullptr;
	etykieta* pierwszaEtykieta = nullptr;
};
bool sprawdzArgument(fstream &plikIn, fstream &plikOut, int argc, char** argv)
{
	for (int n = 1; n < argc; n = n+2)
	{
		if (!argv[n])
		{
			cout << "Nie podano " << n << " argumentu!" << endl;
			cout << "Uruchamiajac program prosze uzyc przelacznikow" << endl;
			cout << "\"-i\" plik wejsciowy" << endl;
			cout << "\"-o\" plik wyjsciowy" << endl;
			return 0;
		}
		else if (argv[n] == (string)"-o")
		{
			if (!argv[n + 1])
			{
				cout << "Nie podano sciezki dla wyjscia!" << endl;
				return 0;
			}
			if (argv[n + 1] && argv[n + 1] != (string)"")
			{
				plikOut.open(argv[n + 1], ios::app);
				if (!plikOut.is_open())
				{
					cout << "Nie udalo sie otworzyc pliku wyjsciowego." << endl;
					return 0;
				}
				if (plikOut.is_open())
					cout << "Udalo sie otworzyc plik wyjsciowy." << endl;
				else
				{
					cout << "Nieznany blad podczas otwerania pliku OUT!" << endl;
					return 0;
				}

			}
			else
			{
				cout << "Nieznany blad!" << endl;
				return 0;
			}
		}
		else if (argv[n] == (string)"-i")
		{
			if (!argv[n + 1])
			{
				cout << "Nie podano sciezki dla wejscia!" << endl;
				return 0;
			}
			if (argv[n + 1] && argv[n + 1] != (string)"")
			{
				plikIn.open(argv[n + 1], ios::in);
				if (!plikIn.is_open())
				{
					cout << "Nie udalo sie otworzyc pliku wejsciowego." << endl;
					return 0;
				}
				if (plikIn.is_open())
					cout << "Udalo sie otworzyc plik wejsciowy." << endl;
				else
				{
					cout << "Nieznany blad podczas otwerania pliku IN!" << endl;
					return 0;
				}
			}
			else
			{
				cout << "Nieznany blad!" << endl;
				return 0;
			}
		}
		else if (argv[n] == (string)"-h")
		{
			cout << "Uruchamiajac program prosze uzyc przelacznikow" << endl;
			cout << "\"-i\" plik wejsciowy" << endl;
			cout << "\"-o\" plik wyjsciowy" << endl;
			return 0;
		}
		else
		{
			cout << "Bledny " << n << " argument" << endl;
			cout << "Uruchamiajac program prosze uzyc przelacznikow" << endl;
			cout << "\"-i\" plik wejsciowy" << endl;
			cout << "\"-o\" plik wyjsciowy" << endl;
			return 0;
		}
	}
	if (plikIn.is_open() && plikOut.is_open())
		return 1;
	else
	{
		return 0;
	}
}
void wyczyscBiale(string &linia) //działa
{
	while (linia[0] == ' ' || linia[0] == '\t')
	{
		if (linia[0] == ' ') //usuwanie spacji z poczatku linii
			linia.erase(0, linia.find(' ') + 1);
		if (linia[0] == '\t') //usuwanie spacji z poczatku linii
			linia.erase(0, linia.find('\t') + 1);
	}
}
void utworzListe(fstream &plikIn, ksiazka* head, int n)
{
	string linia;
	int dobraLinia = 0;
	plikIn.seekg(0);
	plikIn.seekp(0);
	while (getline(plikIn, linia) && dobraLinia < n)
	{
		ksiazka* start = new ksiazka;
		start->autor = "BRAK AUTORA";
		start->tytul = "BRAK TYTULU";
		start->nastepna = nullptr;
		etykieta* etykieta_glowa = start->pierwszaEtykieta;
		etykieta_glowa = nullptr;

		int a = 0;
		int e = 0;

		if (linia.find(';') != -1)
		{
			dobraLinia++;
			wyczyscBiale(linia);
			while (linia.find(';') != -1)
			{
				if (a > 0)
					start->tytul = linia.substr(0, linia.find(';'));
				else
					start->autor = linia.substr(0, linia.find(';'));
				linia.erase(0, linia.find(';') + 1);
				wyczyscBiale(linia);
				a++;
			}
			if (linia.find(',') != -1)
			{
				while (linia.find(',') != -1)
				{
					etykieta_glowa = new etykieta;
					etykieta_glowa->nazwa = linia.substr(0, linia.find(','));
					etykieta_glowa->nastepna = nullptr;
					etykieta_glowa = etykieta_glowa->nastepna;

					linia.erase(0, linia.find(',') + 1);
					wyczyscBiale(linia);
					e++;
				}
				etykieta_glowa = new etykieta;
				etykieta_glowa->nazwa = linia.substr(0, linia.length());
				etykieta_glowa->nastepna = nullptr;
				etykieta_glowa = etykieta_glowa->nastepna;
				linia.erase(0, linia.length());
				wyczyscBiale(linia);
				e++;
			}
			else
			{
				etykieta_glowa = new etykieta;
				etykieta_glowa->nazwa = linia.substr(0, linia.length());
				etykieta_glowa->nastepna = nullptr;
				etykieta_glowa = etykieta_glowa->nastepna;
				linia.erase(0, linia.length());
				wyczyscBiale(linia);
				e++;
			}
		}

	}
}
int liczLinie(fstream &plikIn)//działa
{
	string linia;
	int dobreLinie = 0;
	while (!plikIn.eof())
	{
		getline(plikIn, linia);
		if (linia.find(';') != -1)
			dobreLinie++;
	}
	plikIn.clear();
	plikIn.seekp(0); 
	plikIn.seekg(0);
	return dobreLinie;
}
void drukujListe(ksiazka* head)
{
	;;
}
int main(int argc, char** argv) //argumenty potrzebne do uzywania przelacznikow -i -o
{
	fstream plikIn; //plik ktory otwieram
	fstream plikOut; //plik do ktorego wpisuje dane
	string linia;
	if (sprawdzArgument(plikIn, plikOut, argc, argv) == 0)
	{
		return 0;
		cout << "Zamykanie programu..";
	}
	int iloscLinii = liczLinie(plikIn);
	ksiazka* head = nullptr;
	utworzListe(plikIn, head, iloscLinii);
	drukujListe(head);
	return 0;
}